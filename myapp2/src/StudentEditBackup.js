import React, { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom';

export default function StudentEdit() {
    const [searchParams] = useSearchParams();

    var studentid = searchParams.get("id");

    const [id, setId] = useState(studentid)
    const [name, setName] = useState("")
    const [age, setAge] = useState(0)
    const [city, setCity] = useState("")


    useEffect(() => {

        fetch(`http://localhost:8989/students/${id}`)
        .then(res => res.json())
        .then(result => {
            console.log(result)
            console.table(result)
            setName(result.name)
            setAge(result.age)
            setCity(result.city)
        })

    }, [])


    var putData = () => {
        var data = {
            "id": id,
            "name": name,
            "age": age,
            "city": city
        }

        fetch(`http://localhost:8989/students`, {
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
        .then(result => {
            window.location.href = "/";
            console.log(result)
        })

        // alert(JSON.stringify(data))
        // console.log(data)
    }

    return <>
        Name <input type='text' value={name} onChange={ (e) => {setName(e.target.value)} } /> <br />
        Age <input type='text' value={age} onChange={ (e) => {setAge(e.target.value)} } /> <br />
        City <input type='text' value={city} onChange={ (e) => {setCity(e.target.value)} } /> <br />

        <button onClick={ putData }>Update Student</button>

        <h1>
            ID: {id} <br />
            Name: {name} <br />
            Age: {age} <br />
            City: {city} <br />
        </h1>


        

    </>












    /*
    const [num, setNum] = useState(1)
    const [sqr, setSqr] = useState(1)

    var getSquare = () => {
        setSqr(num * num)
    }

  return (
    <div>
        Number: <input type="text" onChange={ (e) => setNum(e.target.value) } /> 
        <button onClick={() => { getSquare() } }>Get Square</button>
        <h1>Square of {num} is {sqr} </h1>
    </div>
  )
  */
}
