import React, { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom'

export default function StudentEdit() {

    const [searchParams] = useSearchParams();

    const id = searchParams.get("id")

    const [name, setName] = useState("")
    const [age, setAge] = useState(0)
    const [city, setCity] = useState("")


    useEffect(() => {

        fetch(`http://localhost:8989/students/${id}`)
        .then(res => res.json())
        .then(result => {
            console.log(result)
            setName(result.name)
            setAge(result.age)
            setCity(result.city)
        })

    }, [])

    var putData = () => {
        var data = {"id": id, "name": name, "age": age, "city": city}
        alert(JSON.stringify(data))

        fetch(`http://localhost:8989/students`, {
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
        .then(result => {
            alert(result.result)
            window.location.href = "/"
        })

    }

  return (
    <div>
        Name: <input type="text" value={name} onChange={(e) => {setName(e.target.value) }} /> <br/>
        Age: <input type="text" value={age} onChange={(e) => {setAge(e.target.value) }} /> <br/>
        City: <input type="text" value={city} onChange={(e) => {setCity(e.target.value) }} /> <br/>

        <br/>
        <button onClick={ putData } className='btn btn-info'>Update Student Record</button>
        <h1>
            {name}
        </h1>


        <div className="toast" role="alert" aria-live="polite" aria-atomic="true" data-delay="10000">
  <div className="toast-header">
    <img src="..." className="rounded me-2" alt="..." />
    <strong className="me-auto">Bootstrap</strong>
    <small>11 mins ago</small>
    <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close" />
  </div>
  <div className="toast-body">
    Hello, world! This is a toast message.
  </div>
</div>


    </div>
  )
}
