import React from 'react'

export default function Sqr(props) {
  var x = props.x;
  return (
    <div>Square of {x} is {x * x}</div>
  )
}
