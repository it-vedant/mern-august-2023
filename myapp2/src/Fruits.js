import React from 'react'

export default function Fruits() {

    var fruitlist = ["apple", "banana", "chiku", "dangru"]

  return (
    <div>
        <ul>

        {fruitlist.map(x => {
            return <li>{x}</li>
        })}
        </ul>
    </div>
  )
}
