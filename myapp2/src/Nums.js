import React from 'react'

export default function Nums() {

    var arr = [1, 2, 3, 4, 5];

    var arr2 = arr.map(x => {
        return [x * x, x * x * x];
    })

    console.log(arr2);

    var posnum = arr.filter(x => {
        return x > 0
    })

    console.log("Positive numbers: ", posnum)

    var sumofarr = arr.reduce((total, x) => {
        return total + x
    }, 100); // where 100 is initial value of total

    console.log("Total: ", sumofarr)







    // console.log(arr)

    // for(let i = 0; i < arr.length; i++){
    //     console.log(arr[i] * arr[i])
    // }

    return (
        <div>Nums</div>
    )
}
