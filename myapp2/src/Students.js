import React, { useEffect, useState } from 'react'

export default function Students() {

    const [data, setData] = useState([]);

    var refreshListing = () => {
        fetch("http://localhost:8989/students")
        .then(res => res.json())
        .then(result => {
            console.log(result)
            console.table(result)
            setData(result)
        })
    }

    useEffect(() => {

        // setData([
        //     {"name": "shailesh", "age": 25, "city": "Mumbai"}, 
        //     {"name": "mahesh", "age": 21, "city": "Pune"}
        // ]);
        refreshListing()

    }, [])

    var deleteRec = (id) => {
        //alert("From Delete Rec: " + id)
        fetch(`http://localhost:8989/students?id=${id}`, { method: "DELETE" })
        .then(res => res.json())
        .then(result => {
            console.log(result)
            refreshListing();
        })
    }
  return (
    <>
        {/* {JSON.stringify(data)} */}
        <a href='studentadd'>Add New Student</a>
        <table className='table'>
            <thead>
                <th>Sr. No.</th>
                <th>Name</th>
                <th>Age</th>
                <th>City</th>
                <th>Action</th>
            </thead>
            <tbody>
                {
                    data.map((x, i) => {
                        return <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{x.name}</td>
                            <td>{x.age}</td>
                            <td>{x.city}</td>
                            <td>
                                <button onClick={() => { deleteRec(x._id) }} className='btn btn-danger'>Delete</button> &nbsp; 
                                <a className='btn btn-info' href={`studentedit?id=${x._id}`}>Edit</a>
                            </td>
                        </tr>;
                    })
                }
            </tbody>
        </table>
    </>
  )
}
