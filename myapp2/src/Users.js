import React, { useEffect, useState } from 'react'

export default function Users() {
  // var users = [
  //   { name: "Ram", email: "ram@gmail.com" },
  //   { name: "Shyam", email: "shyam@gmail.com" },
  //   { name: "Aryan", email: "aryan@gmail.com" },
  // ]

  const [users, setUsers] = useState([]);
  const [pageno, setPageno] = useState(1);

  useEffect(() => {

    fetch(`https://reqres.in/api/users/?page=${pageno}`)
    .then(res => res.json())
    .then(result => {
        console.log(result.data)
        console.table(result.data)
        setUsers(result.data)
    })

  }, [pageno])

  return (
    <div>
      Page: <input type="text" onChange={(e) => setPageno(e.target.value) } />
      <table className="table table-dark table-striped">
        <tbody>
          {        
          users.map((u, i) => {
            return <tr>
              <td>{u.id}</td>
              <td>{u['first_name'] + " " + u['last_name']}</td>
              <td>{u['email']}</td>
              <td>
                <img src={u.avatar} alt={u.id} />
              </td>
            </tr>
          })
          }
        </tbody>

        <tfoot>
          <tr>
            <td colSpan="3">Mein to footer hu mujhe kon yaad rakhega :( </td>
          </tr>
        </tfoot>











        <thead>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Avatar</th>
        </thead>
      </table>








      {/* {JSON.stringify(users)} */}
      {/* {users.map((u, index) => {

            return <div key={index}>
              Name: {u.name} Email: {u.email}
            </div>

        })} */}
    </div>
  )
}
