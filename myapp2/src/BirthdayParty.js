import React from 'react'

export default function BirthdayParty() {

    var cakepromise = new Promise(res => {
        console.log("Mere karan arjun cake lekar aayenge")

        setTimeout(() => {
            return res("Cake ready hai...")
        }, 5000)
    });

    cakepromise.then(result => {
        console.log(result)
    })

    //CW create dress promise which will execute after 20 sec
    var dresspromise = new Promise(res => {
        console.log("Dress banana start ho gaya hai")
        setTimeout(() => {
            return res("Dress ban gaya hai bhai lekar jao")
        }, 20000)
    })

    dresspromise.then(res => {
        console.log(res)
    })


    //CW create drinks promise which will execute after 10 sec

    var drinkspromise = new Promise(res => {
        console.log("Mayur drinks lene gaya hai")
        setTimeout(() => {
            return res("Omkar bhai drinks ka jugad ho gaya let's start party")
        }, 10000)
    })

    drinkspromise.then(res => {
        console.log(res)
    })

    console.log("run before settimeout")


  return (
    <div>BirthdayParty</div>
  )
}
