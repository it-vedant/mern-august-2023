import React, { useState } from 'react'

export default function StudentAdd() {

    const [name, setName] = useState("")
    const [age, setAge] = useState(0)
    const [city, setCity] = useState("")

    var postData = () => {
        var data = {
            "name": name,
            "age": age,
            "city": city
        }

        fetch(`http://localhost:8989/students`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(res => res.json())
        .then(result => {
            window.location.href = "/"
            // window.location.reload();
            console.log(result)
        })

        // alert(JSON.stringify(data))
        // console.log(data)
    }

    return <>
        Name <input type='text' onChange={ (e) => {setName(e.target.value)} } /> <br />
        Age <input type='text' onChange={ (e) => {setAge(e.target.value)} } /> <br />
        City <input type='text' onChange={ (e) => {setCity(e.target.value)} } /> <br />

        <button onClick={ postData }>Add Student</button>

        <h1>
            Name: {name} <br />
            Age: {age} <br />
            City: {city} <br />
        </h1>
    </>












    /*
    const [num, setNum] = useState(1)
    const [sqr, setSqr] = useState(1)

    var getSquare = () => {
        setSqr(num * num)
    }

  return (
    <div>
        Number: <input type="text" onChange={ (e) => setNum(e.target.value) } /> 
        <button onClick={() => { getSquare() } }>Get Square</button>
        <h1>Square of {num} is {sqr} </h1>
    </div>
  )
  */
}
