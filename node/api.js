const express = require("express")
const app = express()
const mongoose = require("mongoose");

var cors = require('cors');
app.use(cors());

const conn_str = "mongodb+srv://pooja:pooja@cluster0.b92feac.mongodb.net/college?retryWrites=true&w=majority"

mongoose.connect(conn_str, { useNewUrlParser: true , useUnifiedTopology: true})
	.then( () => console.log("Connected successfully...") )
	.catch( (err) => console.log(err) );

const userSchema = new mongoose.Schema({
	name: String,
	age: Number,
	city: String
});

const student = new mongoose.model("students", userSchema);

app.get("/students", async (req, res) => {
	let data = await student.find();
	res.send(data);
})

app.delete("/students", async (req, res) => {
    var result = await student.deleteOne({_id: req.query.id})
	res.send(result)
})


app.use(express.json())

app.post("/students", async (req, res) => {

    console.log(req.body)

    let obj = new student(req.body)
    var result = obj.save();

    // var tempstd = {name: "sohel", age: 20, city: "New Mumbai"}
    // let obj = new student(tempstd)
    // var result = obj.save();

    res.send({"result": "Record inserted successfully...."})
})


app.put("/students", async (req, res) => {

    // var result = await student.updateOne({ _id: req.body.id }, { $set : req.body }) // on linux you can skip Double quote for $set
    var result = await student.updateOne({ _id: req.body.id }, { "$set" : req.body })

    res.send({"result": "Record updated successfully...."})

})

//Fetch single record
//URL: http://localhost:8989/students/65227b23f81d84612b839e1f
app.get("/students/:id", async(req, res) => {

    console.log(req.params)

    let data = await student.findOne({ _id: req.params.id });

	res.send(data);
})


app.get("/", (req, res) => {
    res.send("Hello from express api")
})

app.listen(8989, () => {
    console.log(`Listening to port 8989`)
})






















// var test = () => {
//     console.log("Hello from test function var lambda...")
// }

// test();

// //CW create add(x, y) function with 2 parameters which will print addition of 2 numbers
// var add = (x, y) => {
//     console.log(x + y)
// }

// //CW create sub(x, y) function with 2 parameters which will print subtraction of 2 numbers
// var sub = (x, y) => {
//     console.log(x - y)
// }

// //CW create calc(x, y, op) function with 3 parameters which will accept 2 numbers and one callback fun
// var calc = (a, b, op) => {
//     op(a, b)
// }

// calc(5, 6, add);

// calc(5, 6, (a, b) => {
//     console.log(a * b)
// })



