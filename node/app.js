var express = require("express")
var app = express();

app.get("/", (req, res) => {
    res.send("I am from node app...");
})






//CW Create a web service sqr 
//which receives a number from query param 
//and print square of number
app.get("/sqr", (req, res) => {
    console.log(req.query)
    var x = req.query.num
    res.send(`Square of ${x} is ${x * x}`);
})




//CW Crete a web service add which will 
//receive 2 query params
//and return addition in json format
app.get("/add", (req, res) => {
    var x = parseInt(req.query.num1)
    var y = parseInt(req.query.num2)

    var result = {
        "status": true,
        "addition": x + y
    }

    res.send(result);
})

//CW Create a web service sub which will
//receive 2 numbers from raw json
//and return subtraction of 2 numbers
//NOTE: use post method and hit service from postman
app.use(express.json())
app.post("/sub", (req, res) => {
    console.log(req.body)

    var x = req.body.num1
    var y = req.body.num2

    var result = {
        "status": true,
        "subtraction": x - y
    }

    res.send(result);
})



app.put("/mul", (req, res) => {
    console.log(req.body)

    var x = req.body.num1
    var y = req.body.num2

    var result = {
        "status": true,
        "multiplication": x * y
    }

    res.send(result);
})


app.delete("/div", (req, res) => {
    console.log(req.body)

    var x = req.body.num1
    var y = req.body.num2

    var result = {
        "status": true,
        "division": x / y
    }

    res.send(result);
})








app.listen(8989, () => {
    console.log(`Listening to port 8989`);
})