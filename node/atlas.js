const express = require("express")
const app = express()
const mongoose = require("mongoose");
var cors = require('cors');
app.use(cors());

const conn_str = "mongodb+srv://pooja:pooja@cluster0.b92feac.mongodb.net/college?retryWrites=true&w=majority"

mongoose.connect(conn_str, { useNewUrlParser: true , useUnifiedTopology: true})
	.then( () => console.log("Connected successfully...") )
	.catch( (err) => console.log(err) );


const userSchema = new mongoose.Schema({
    name: String,
    age: Number,
    city: String
});

const students = new mongoose.model("students", userSchema);

app.get("/students", async (req, res) => {
    let data = await students.find()
    // let data = await students.find({"name":"ram"}); //with where clause
    res.send(data)
})


app.get("/", (req, res) => {
    res.send("Hello Node App")
})


app.listen(8989, () => {
    console.log(`Listening to port 8989`)
})