add = function(x, y) {
    console.log(x + y);
}

// add(5, 6);
// add(7, 8);

function calc(a, b, op) {
    op(a, b);
}

calc(5, 6, add);
//CW create sub function which print subtraction of 2 numbers
//call sub function as a callback function by passing as argument to calc function

sub = (x, y) => { 
    console.log(`${x} - ${y} = ${x-y}`) 
};

calc(10, 7, sub)

calc(2, 3, function(x, y) {
    console.log(x * y);
})


//CW create callback function using fat array to find division of 2 numbers








