export const sleep = (milisec) => {
    return new Promise(r => setTimeout(r, milisec*1000))
}