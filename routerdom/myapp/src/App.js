import logo from './logo.svg';
import './App.css';
import Users from './Users';
import Contactus from './Contactus';
import Pagenotfound from './Pagenotfound';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from './Layout';
import Home from './Home';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />} >
            <Route index element={<Home />} />
            <Route path="/home" element={"Hello Home"} />
            <Route path="/about-us" element={<h1>Hello from About us</h1>} />
            <Route path="/contact-us" element={<Contactus />} />

            <Route path="/users" element={<Users />} />

            <Route path="*" element={<Pagenotfound />} />
          </Route>
        </Routes>
      </BrowserRouter>
      {/* <Users /> */}
    </>
  );
}

export default App;
