import React, { useEffect, useState } from 'react'
import User from './User';
import axios from 'axios';

export default function Users() {

    const [users, setUsers] = useState(null)
    const [pageno, setPageno] = useState(1)


    useEffect(() => {

        var url = `https://reqres.in/api/users?page=${pageno}`


        // fetch(url)
        //     .then(res => res.json())
        //     .then(result => {

        //         console.table(result.data)
        //         setUsers(result.data)

        //     })

        axios.get(url)
            .then(response => {
                setUsers(response.data.data);
            })

    }, [pageno]);


    return (
        <div>
            Pageno <input type="text" onChange={(e) => setPageno(e.target.value)} />

            {
                users.map((u, i) => {
                    return <User user={u} />
                })
            }



            {/* <div>
                {JSON.stringify(users)}
            </div> */}
            <h1>Current Page is {pageno}</h1>

        </div>
    )
}
