import React from 'react'

export default function User(props) {
    return (
        <div>
            <div className="card" style={{ width: '18rem' }}>
                <img src={props.user.avatar} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">{props.user.first_name}</h5>
                    <p className="card-text">{props.user.email}</p>
                    <a href="#" className="btn btn-primary">View Details</a>
                </div>
            </div>

        </div>
    )
}
